sap.ui.define(
	[
        "sap/ui/core/UIComponent"
    ],
	function(UIComponent) {
		"use strict";

		/**
		 * @name        ui5expresstemplate.Component
         * @alias       myCoolPlugin
		 * @instance
		 * @public
		 * @class
         * @todo please replace myCoolPlugin with a meaningfulname.
		 * <p></p>
		 */
		const myCoolPlugin = UIComponent.extend(
			"ui5expresstemplate.Component",
			/**@lends ui5expresstemplate.Component.prototype **/ {
				metadata: {
					manifest: "json"
				}
			}
		);

		/**
		 * @method init
		 * @public
		 * @instance
		 * @memberof ui5expresstemplate.Component
         * @todo Some plugins can be faceless, in which case you can remove the addHeaderEndItem.
		 * <p> </p>
		 * 
		 */
		myCoolPlugin.prototype.init = function(){
            this._headerItem = sap.ushell.Container.getRenderer("fiori2").addHeaderEndItem(
                "sap.ushell.ui.shell.ShellHeadItem", 
                {
                    icon: "sap-icon://warning",
                    press: this.onPluginIconPress.bind(this),
                    visible: true,
                    text: this.getModel("i18n").getResourceBundle().getText("issuelogger.title")
                }, 
                true, 
                false,
                ["app","home"] //show the header-item both in apps and in the home screen
            );
		};	

		/**
		 * @method destroy
		 * @public
		 * @instance
		 * @memberof ui5expresstemplate.Component
		 * <p>object destructor: remove any pending event handlers and taskrepeaters</p>
		 */
		myCoolPlugin.prototype.destroy = function(){
		};

		/**
		 * @method onPluginIconPress
		 * @public
		 * @instance
		 * @memberof ui5expresstemplate.Component
         * @param {event} event
         * @todo if your plugin is faceless (no-ui) then you probably don't need this method
		 * <p></p>
		 */
		myCoolPlugin.prototype.onPluginIconPress = function(event){
		};

		return myCoolPlugin;
	}
);
